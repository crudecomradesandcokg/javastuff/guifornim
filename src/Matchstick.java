import processing.core.PApplet;

public class Matchstick{
    private float xAxe, yAxe, rectWidth, rectHeight;
    private PApplet sketch = new PApplet();
    private int fillColor= sketch.color(67,60,5);
    private int groundColor= sketch.color(0,255,0);

    public Matchstick(float x, float y, float rectWidth, float rectHeight){
        this.xAxe = x;
        this.yAxe = y;
        this.rectWidth = rectWidth;
        this.rectHeight = rectHeight;
    }
    public Matchstick(float x,float y){
        this.xAxe= x;
        this.yAxe= y;
        this.rectWidth = (float)10;
        this.rectHeight= (float)100;
    }
    public void render(){
        sketch.fill(fillColor);
        sketch.rect(xAxe, yAxe, rectWidth, rectHeight);
    }
    public void reaction(){
        sketch.fill(groundColor);
        sketch.rect(xAxe, yAxe, rectWidth, rectHeight);
    }

    public float getX(){
        return xAxe;
    }
    public float getY(){
        return yAxe;
    }
    public float getWidth(){
        return rectWidth;
    }
    public int getFillColor(){
        return fillColor;
    }
    public float getHeight(){
        return rectHeight;
    }
    public void setX(float x){
        this.xAxe = x;
    }
    public void setY(float y){
        this.yAxe = y;
    }
}