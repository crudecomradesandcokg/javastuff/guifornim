import java.util.ArrayList;
import java.util.Random;

public class GameMechanics{
    public int[] randomSetup(int... maxN) {
        Random r = new Random();
        int[] rows = new int[maxN.length];
        for(int i = 0; i < maxN.length; i++) {
            rows[i] = r.nextInt(maxN[i]) + 1;
        }
        return rows;
    }
    
    public ArrayList<Move> autoplay(NimGame nim) {
        ArrayList<Move> moves = new ArrayList<>();
        while (!nim.isGameOver()) {
            Move m = nim.bestMove();
            moves.add(m);
            nim = nim.play(m);
        }
        return moves;
    }
    
    //public boolean simulateGame(int... maxN) {
    //    NimGame nim = Nim.of(randomSetup(maxN)); 
    //    ArrayList<Move> moves = autoplay(nim);
    //    return (NimGame.isWinning(nim.rows) && (moves.size() % 2) == 1) ||
    //           (!NimGame.isWinning(nim.rows) && (moves.size() % 2) == 0); 
    //}
}