import java.util.ArrayList;

import processing.core.*;


public class View extends PApplet{
	public ArrayList<Matchstick> matchsticks;
    private Matchstick spieler1 = new Matchstick((float)60.0,(float)60.0, (float)10,(float) ((float)100));
	private int backGroundColor = color(15,67,5);

    public void settings() {
		size(1000, 1000);
	}

	public void draw(){
		background(backGroundColor);
		fill(spieler1.getFillColor());
		if(spieler1.getX() == mouseX && spieler1.getY() == mouseY){
			spieler1.reaction();    
		  }
		  else{
		   spieler1.render();
		}
	}
	public ArrayList<Matchstick> matchstickAdder(int amount){
		ArrayList<Matchstick> temp= new ArrayList<Matchstick>();
		for(int i = 0; i < amount; i++){
			temp.add(new Matchstick(i*100,60));
		}
		return temp;
	}

    public static void main(String[] passedArgs) {
		String[] appletArgs = new String[] { "View" };
		View myView = new View();
		PApplet.runSketch(appletArgs, myView);
    }
}